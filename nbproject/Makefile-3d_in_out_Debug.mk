#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=3d_in_out_Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/cbc092bd/HEVC_Decoder.o \
	${OBJECTDIR}/_ext/1dd2946a/NvDecoder.o \
	${OBJECTDIR}/_ext/62146d42/NvEncoder.o \
	${OBJECTDIR}/_ext/62146d42/NvEncoderCuda.o \
	${OBJECTDIR}/_ext/cbc092bd/TCP_stream.o \
	${OBJECTDIR}/_ext/cbc092bd/VideoStabilizer.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-L/usr/local/cuda/lib64/stubs -L/usr/local/include/opencv4 -lavutil -lavcodec -lboost_system -lboost_thread -lopencv_core -ldl -lopencv_highgui -lopencv_imgcodecs -lopencv_imgproc -lnvcuvid -lcuda -lavdevice /home/kid/NetBeansProjects/3d_in_out/cuda_functions/libdistortion.so -lopencv_cudawarping -lopencv_cudaarithm -lopencv_cudaoptflow -lopencv_cudaimgproc -lopencv_calib3d

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3d_in_out

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3d_in_out: /home/kid/NetBeansProjects/3d_in_out/cuda_functions/libdistortion.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3d_in_out: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3d_in_out ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/cbc092bd/HEVC_Decoder.o: /home/kid/NetBeansProjects/3d_in_out/HEVC_Decoder.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/cbc092bd
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/local/include/opencv4 -I/usr/local/cuda -I/usr/local/cuda/include -INvEncoder -INvDecoder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/cbc092bd/HEVC_Decoder.o /home/kid/NetBeansProjects/3d_in_out/HEVC_Decoder.cpp

${OBJECTDIR}/_ext/1dd2946a/NvDecoder.o: /home/kid/NetBeansProjects/3d_in_out/NvDecoder/NvDecoder.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/1dd2946a
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/local/include/opencv4 -I/usr/local/cuda -I/usr/local/cuda/include -INvEncoder -INvDecoder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1dd2946a/NvDecoder.o /home/kid/NetBeansProjects/3d_in_out/NvDecoder/NvDecoder.cpp

${OBJECTDIR}/_ext/62146d42/NvEncoder.o: /home/kid/NetBeansProjects/3d_in_out/NvEncoder/NvEncoder.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/62146d42
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/local/include/opencv4 -I/usr/local/cuda -I/usr/local/cuda/include -INvEncoder -INvDecoder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/62146d42/NvEncoder.o /home/kid/NetBeansProjects/3d_in_out/NvEncoder/NvEncoder.cpp

${OBJECTDIR}/_ext/62146d42/NvEncoderCuda.o: /home/kid/NetBeansProjects/3d_in_out/NvEncoder/NvEncoderCuda.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/62146d42
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/local/include/opencv4 -I/usr/local/cuda -I/usr/local/cuda/include -INvEncoder -INvDecoder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/62146d42/NvEncoderCuda.o /home/kid/NetBeansProjects/3d_in_out/NvEncoder/NvEncoderCuda.cpp

${OBJECTDIR}/_ext/cbc092bd/TCP_stream.o: /home/kid/NetBeansProjects/3d_in_out/TCP_stream.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/cbc092bd
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/local/include/opencv4 -I/usr/local/cuda -I/usr/local/cuda/include -INvEncoder -INvDecoder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/cbc092bd/TCP_stream.o /home/kid/NetBeansProjects/3d_in_out/TCP_stream.cpp

${OBJECTDIR}/_ext/cbc092bd/VideoStabilizer.o: /home/kid/NetBeansProjects/3d_in_out/VideoStabilizer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/cbc092bd
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/local/include/opencv4 -I/usr/local/cuda -I/usr/local/cuda/include -INvEncoder -INvDecoder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/cbc092bd/VideoStabilizer.o /home/kid/NetBeansProjects/3d_in_out/VideoStabilizer.cpp

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/local/include/opencv4 -I/usr/local/cuda -I/usr/local/cuda/include -INvEncoder -INvDecoder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} -r ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libdistortion.so
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/3d_in_out

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
