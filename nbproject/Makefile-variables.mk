#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# 3d_in_out_Debug configuration
CND_PLATFORM_3d_in_out_Debug=GNU-Linux
CND_ARTIFACT_DIR_3d_in_out_Debug=dist/3d_in_out_Debug/GNU-Linux
CND_ARTIFACT_NAME_3d_in_out_Debug=3d_in_out
CND_ARTIFACT_PATH_3d_in_out_Debug=dist/3d_in_out_Debug/GNU-Linux/3d_in_out
CND_PACKAGE_DIR_3d_in_out_Debug=dist/3d_in_out_Debug/GNU-Linux/package
CND_PACKAGE_NAME_3d_in_out_Debug=3dinout.tar
CND_PACKAGE_PATH_3d_in_out_Debug=dist/3d_in_out_Debug/GNU-Linux/package/3dinout.tar
# 3d_in_out_Release configuration
CND_PLATFORM_3d_in_out_Release=GNU-Linux
CND_ARTIFACT_DIR_3d_in_out_Release=dist/3d_in_out_Release/GNU-Linux
CND_ARTIFACT_NAME_3d_in_out_Release=3d_in_out
CND_ARTIFACT_PATH_3d_in_out_Release=dist/3d_in_out_Release/GNU-Linux/3d_in_out
CND_PACKAGE_DIR_3d_in_out_Release=dist/3d_in_out_Release/GNU-Linux/package
CND_PACKAGE_NAME_3d_in_out_Release=3dinout.tar
CND_PACKAGE_PATH_3d_in_out_Release=dist/3d_in_out_Release/GNU-Linux/package/3dinout.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
