/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TCP_stream.h
 * Author: kid
 *
 * Created on April 2, 2020, 5:23 PM
 */

#ifndef TCP_STREAM_H
#define TCP_STREAM_H

#include <stdio.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <string.h>
#include <boost/thread.hpp>

class TCP_stream {
public:
    TCP_stream();
    TCP_stream(const TCP_stream& orig);
    bool load(int);
    bool stream_raw_data(uint8_t *, int);
    void setPositionbuffer(char *);
    void receive_marker_position();
    virtual ~TCP_stream();
private:
    int sockfd, newsockfd, portno;
    struct sockaddr_in serv_addr, cli_addr;
    socklen_t clilen;
    char * udp_to_station;

};

#endif /* TCP_STREAM_H */

