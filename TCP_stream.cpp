/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TCP_stream.cpp
 * Author: kid
 * 
 * Created on April 2, 2020, 5:23 PM
 */

#include "TCP_stream.h"

TCP_stream::TCP_stream() {
    sockfd=-1;
}

TCP_stream::TCP_stream(const TCP_stream& orig) {
}

TCP_stream::~TCP_stream() {
    close(sockfd);
}

bool TCP_stream::stream_raw_data(uint8_t *p_payload, int i_frame_size) {
    //printf("start packet stream");
    if (newsockfd < 0)
        return true;
    int n = send(newsockfd,p_payload,i_frame_size,0);
    printf("packet sent");
    
    if (n < 0) {
         perror("ERROR writing stream to socket");
         close(newsockfd);
         load(portno);
         return false;
    }
    return true;
}

void TCP_stream::setPositionbuffer(char * udp_buffer){
    udp_to_station = udp_buffer;
}

void TCP_stream::receive_marker_position(){
    int n;
    int fail_threshold=100;
    while (newsockfd > -1 && fail_threshold>0){
        n = read(newsockfd, udp_to_station,255);
        printf("reading %d bytes string>>%s<<",n , udp_to_station);
        if (n < 0) {
            printf("ERROR reading from socket");
            fail_threshold--;
        } 
        else {
            fail_threshold=100;
//            printf("Here is the message: %s\n",udp_to_station);
        }
    }
}

bool TCP_stream::load(int port_number){
/* First call to socket() function */     //mine
  sockfd = socket(AF_INET, SOCK_STREAM, 0); //mine
  
  if (sockfd < 0) {
      perror("ERROR opening socket");
      return false;
  }
  
  /* Initialize socket structure */
  bzero((char *) &serv_addr, sizeof(serv_addr));
  portno = port_number;//5001;
   
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(portno);
   
  /* Now bind the host address using bind() call.*/
  if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
      perror("ERROR on binding");
      return false;
  }
      
   /* Now start listening for the clients, here process will
      * go in sleep mode and will wait for the incoming connection
   */
   
  printf("Waiting connection on 1234");
  listen(sockfd,5);
  clilen = sizeof(cli_addr);
  
  int no_delay_flag=1;
  setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY,(char *) &no_delay_flag, sizeof(int));
   
   /* Accept actual connection from the client */
  newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen);

  if (newsockfd < 0) {
      perror("ERROR on accept");
      return false;
  } else {
      // creating thread for receiving data from device marker
      //boost::thread t_receive_marker_position(&TCP_stream::receive_marker_position, this);
  }

}