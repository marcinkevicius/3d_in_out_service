/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   HEVC_Decoder.h
 * Author: kid
 *
 * Created on April 1, 2020, 3:08 AM
 */

#ifndef HEVC_DECODER_H
#define HEVC_DECODER_H
#define HEVC_INBUF_SIZE 512*30 //16384                                                           /* number of bytes we read per chunk */

#include "NvDecoder/NvDecoder.h"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavutil/avutil.h>
}
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <boost/thread/thread.hpp>

class HEVC_Decoder {
    
public:
    HEVC_Decoder(int, int);
    HEVC_Decoder(const HEVC_Decoder& orig);
    virtual ~HEVC_Decoder();
    bool load(int port_number);
    bool readFrame();                                                                      /* read a frame if necessary */
    bool update(bool& needsMoreBytes);                                                     /* internally used to update/parse the data we read from the buffer or file */
    void decodeFrame(uint8_t* data, int size);                                             /* decode a frame we read from the buffer */
    
    NvDecoder* dec;
    bool cb_frame;
    uint8_t **ppFrame;
private:
    int nts;
    CUcontext cuContext;
    int nFrameReturned;
    int64_t *pTimestamp;
    AVCodec* codec;                                                                        /* the AVCodec* which represents the H264 decoder */
    AVCodecContext* codec_context;                                                         /* the context; keeps generic state */
    AVCodecParserContext* parser;                                                          /* parser that is used to decode the h264 bitstream */
    AVFrame* picture;                                                                      /* will contain a decoded picture */
    
    int sockfd, newsockfd, portno;    //mine
    struct sockaddr_in serv_addr, cli_addr;   //mine
    uint8_t inbuf[HEVC_INBUF_SIZE + FF_INPUT_BUFFER_PADDING_SIZE];                         /* used to read chunks from the file */
    socklen_t clilen;
    
    char socket_buffer[HEVC_INBUF_SIZE];             //mine
    int no_data_count;
    int readBuffer();
    std::vector<uint8_t> buffer;                                                           /* buffer we use to keep track of read/unused bitstream data */
    int buffer_size_per_frame;                                                              /* average buffer size per frame over 30 frames */
};

#endif /* HEVC_DECODER_H */

