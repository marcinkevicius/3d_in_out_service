### video streamer to VR kit ###

Gets stereo video from drone, prepares and pushes out to vr kit

# workflow #
* get video from drone
* Decodes on gpu with NV decoder
* concatinates, modifies, and stabilizes video frames
* encodes back to h265 stream
* pushes stream to VR kit
* get VR kit position as callback
* pushes vr position to antenna app (jetson_pixhawk)

# libraries #
* opencv 4.0
* nvidia encoder/decoder SDK
* cuda > 7.5
* Nvidia Turing gpu chip !!!