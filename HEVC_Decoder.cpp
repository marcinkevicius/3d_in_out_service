/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   HEVC_Decoder.cpp
 * Author: kid
 * 
 * Created on April 1, 2020, 3:08 AM
 */

#include "HEVC_Decoder.h"

HEVC_Decoder::HEVC_Decoder(int input_stream_w, int input_stream_h)  
  :codec(NULL)
  ,codec_context(NULL)
  ,parser(NULL)
//  ,fp(NULL)
//  ,frame(0)
  ,cb_frame(0)
//  ,frame_timeout(0)
//  ,frame_delay(0)
//  ,code_word(0)
//  ,frame_time_stamp(0)
  ,nFrameReturned(0)
  ,nts(0)
,cuContext(NULL)
,buffer_size_per_frame(HEVC_INBUF_SIZE)
{
    avcodec_register_all();
    int iGpu = 0;
    cuInit(0);
    int nGpu = 0;
    cuDeviceGetCount(&nGpu);
    if (iGpu < 0 || iGpu >= nGpu)
    {
        std::cout << "GPU ordinal out of range. Should be within [" << 0 << ", " << nGpu - 1 << "]" << std::endl;
    }
    CUdevice cuDevice = 0;
    cuDeviceGet(&cuDevice, iGpu);  
  
  cuCtxCreate(&cuContext, 0, cuDevice);
  dec = new NvDecoder (cuContext, input_stream_w, input_stream_h, false, cudaVideoCodec_HEVC, NULL, true);
}

HEVC_Decoder::HEVC_Decoder(const HEVC_Decoder& orig) {
}

HEVC_Decoder::~HEVC_Decoder() {
    close(newsockfd);
    delete dec;
}

bool HEVC_Decoder::load(int port_number)
{
 
  //TODO : codec is not accessible for 20s after program exits. WTF? :/
  codec = avcodec_find_decoder(AV_CODEC_ID_HEVC);
  if(!codec) {
    printf("Error: cannot find the hevc codec: \n");
    return false;
  }
 
  codec_context = avcodec_alloc_context3(codec);
 
  if(codec->capabilities & CODEC_CAP_TRUNCATED) {
    codec_context->flags |= CODEC_FLAG_TRUNCATED;
  }
 
  if(avcodec_open2(codec_context, codec, NULL) < 0) {
    printf("Error: could not open codec.\n");
    return false;
  }
  
  /* First call to socket() function */     //mine
  sockfd = socket(AF_INET, SOCK_STREAM, 0); //mine
  
  if (sockfd < 0) {
      perror("ERROR opening socket");
      return false;
  }
  
  /* Initialize socket structure */
  bzero((char *) &serv_addr, sizeof(serv_addr));
  portno = port_number;//5001;
   
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(portno);
   
  /* Now bind the host address using bind() call.*/
  if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
      perror("ERROR on binding");
      return false;
  }
      
   /* Now start listening for the clients, here process will
      * go in sleep mode and will wait for the incoming connection
   */
   
  listen(sockfd,5);
  clilen = sizeof(cli_addr);
   
   /* Accept actual connection from the client */
  newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen);
	
  if (newsockfd < 0) {
      perror("ERROR on accept");
      return false;
  }
   
   /* If connection is established then start communicating */
  bzero(socket_buffer, HEVC_INBUF_SIZE);
    
  picture = av_frame_alloc();
  parser = av_parser_init(AV_CODEC_ID_HEVC);
 
  if(!parser) {
    printf("Erorr: cannot create HEVC parser.\n");
    return false;
  }

  // kickoff reading...
  no_data_count=0; // set no data error to 0
  printf("looks ok \n");
  readBuffer();
 
  return true;
}

int HEVC_Decoder::readBuffer() {
  int bytes_read = read(newsockfd, inbuf, HEVC_INBUF_SIZE );
  //printf("socket status %d, bytes read %d \n", newsockfd, bytes_read);
  int bytes_read_all = bytes_read;

  if(bytes_read) {
    //  copy bytes to buffer
    std::copy(inbuf, inbuf + bytes_read, std::back_inserter(buffer));
  }
  
  if ((bytes_read_all) < HEVC_INBUF_SIZE ){
    boost::this_thread::sleep( boost::posix_time::milliseconds(10) );
  }
  
  if (bytes_read){
      no_data_count=0;
  } else no_data_count++;
  
  return bytes_read;
}

bool HEVC_Decoder::readFrame() {
 
  bool needs_more = false;
 
  while(!update(needs_more)) { 
    if(needs_more) {
      readBuffer();
    }
    
    //std::cout << "no_data_count -> " << no_data_count;
    if (no_data_count>100){ //if data if failing to arrive, then close the connection
          return false;
      }
  }
 
  return true;
}

bool HEVC_Decoder::update(bool& needsMoreBytes) {
 
  needsMoreBytes = false;
 
  if (!newsockfd){ //mine
    printf("Cannot update .. file not opened...\n");
    return false;
  }
 
  //if(buffer.size() == 0) {
  if(buffer.size() < (buffer_size_per_frame * 0.9)) {
    needsMoreBytes = true;
    //printf("buffer size %lu. %d . average BS %lu \n", buffer.size(), dec_name, buffer_size_per_frame);
    return false;
  }
  
  //if (dec_name == 5) printf("buffer size %lu. %d . average BS %d \n", buffer.size(), dec_name, buffer_size_per_frame);
 
  uint8_t* data = NULL;
  int size = 0;
  int len = av_parser_parse2(parser, codec_context, &data, &size, 
                             &buffer[0], buffer.size(), 0, 0, AV_NOPTS_VALUE);
 
  // "size"  - set to size of parsed buffer or zero if not yet finished
  // len - the number of bytes of the input bitstream used
  if(size == 0 && len >= 0) {

//    if (dec_name == 5) printf("size 0 len >= 0 len is %d \n", len);
      
    needsMoreBytes = true;
    return false;
  }
  
//  if (dec_name == 5) printf("len is %d size is %d \n", len, size);
 
  if(len) {
      
//    if (dec_name == 5) printf("if ( len ) len is %d \n", len);
    
    buffer_size_per_frame = ((buffer_size_per_frame*30)+buffer.size())/31;
      
    decodeFrame(&buffer[0], size);
    buffer.erase(buffer.begin(), buffer.begin() + len);
    return true;
  }
  
//  if (dec_name == 5) printf("| end of function | \n");
 
  return false;
}

void HEVC_Decoder::decodeFrame(uint8_t* data, int size) {
 
    int got_picture = 0;
    int len = 0;
 

        // high_resolution_clock::time_point t1 = high_resolution_clock::now();
        dec->Decode(data, size, &ppFrame, &nFrameReturned, CUVID_PKT_ENDOFPICTURE, &pTimestamp, nts++);
        // high_resolution_clock::time_point t2 = high_resolution_clock::now();
        
        //auto duration = duration_cast<microseconds>( t2 - t1 ).count();
        //if (dec_name == 5 )printf("decoding duration %ld ", duration);
      
        if(nFrameReturned == 0) {
            printf("Error while decoding a frame.\n");
        }

        if (nFrameReturned == 0){
            return;
        } else
            cb_frame = true;

    //++frame;
}