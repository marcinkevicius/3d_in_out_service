/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: kid
 *
 * Created on April 1, 2020, 2:49 AM
 */

#include <cstdlib>
#include <opencv2/opencv.hpp>
#include <opencv2/cudev/ptr2d/gpumat.hpp>
#include "HEVC_Decoder.h"
#include "NvEncoder/NvEncoderCuda.h"
#include "TCP_stream.h"
#include "cuda_functions/distortion.h"
#include "VideoStabilizer.h"

using namespace std;
using namespace cv;

const int input_stream_w = 3840;
const int input_stream_h = 1080;
const int output_stream_w = 1920;
const int output_stream_h = 540;
//const int output_stream_w = 3840;
//const int output_stream_h = 1080;
#define MAT_STACK_SIZE 3
#define ENABLE_ANAGLYPTH 1
#define LIMIT_INPUT_STREAM 0
#define ENABLE_ENCODER 1
#define SWAP_CAMERAS 0
#define STABILIZE_FRAMES 1
/*
 * 
 */

Mat w_decode_mat[MAT_STACK_SIZE];
Mat w_encode_mat[MAT_STACK_SIZE];
cuda::GpuMat w_left_mat[MAT_STACK_SIZE];
cuda::GpuMat w_right_mat[MAT_STACK_SIZE];
int w_decode_mat_status[MAT_STACK_SIZE];  // status 0 = free, 1-occupied by dec, 2 - ready for processing
int w_encode_mat_status[MAT_STACK_SIZE];  // status 0 = free for imput, 1-occupied by enc

boost::mutex w_mutex;
bool finish=false;

void main_decode_t() {
    Mat m_null;     // discard mat data on this war
    int free_mat_array=-1;  // indicates index where decoder can push new mat data

    std::chrono::high_resolution_clock::time_point frame_ending_time = std::chrono::high_resolution_clock::now();
    std::chrono::high_resolution_clock::time_point time_now = frame_ending_time;
    
    HEVC_Decoder * ddec = new HEVC_Decoder(input_stream_w, input_stream_h);
    if (ddec->load(5001)){
        while (ddec->readFrame()) {
            if (ddec->cb_frame) {    // if we have frame ready to be processed
                ddec->cb_frame = false;

                free_mat_array=-1;
                for (int i=0; i< MAT_STACK_SIZE; i++ ){
                    if (w_decode_mat_status[i] == 0) {
                        w_decode_mat_status[i]=1;
                        free_mat_array = i;
                        break;
                    }
                }
                if (free_mat_array > -1){
                    w_decode_mat[free_mat_array] = cv::Mat(ddec->dec->GetHeight() * 3/2, ddec->dec->GetWidth(), CV_8UC1, ddec->ppFrame[0]);
                    w_decode_mat_status[free_mat_array]=2;
                } else { // if no free array to deploy new mat data - then discard data
                    m_null = cv::Mat(ddec->dec->GetHeight() * 3/2, ddec->dec->GetWidth(), CV_8UC1, ddec->ppFrame[0]);
                    m_null.release();
                    printf("No free array to store new mat data");
                }
                
                #if LIMIT_INPUT_STREAM
                time_now = std::chrono::high_resolution_clock::now();
                int time_diff = std::chrono::duration_cast<std::chrono::milliseconds>( time_now - frame_ending_time ).count();

                printf("time_diff %d \n", time_diff);
                if (time_diff< 40 && time_diff > 0){
                    boost::this_thread::sleep(boost::posix_time::milliseconds(40 - time_diff));
                }

                frame_ending_time = time_now;

                // enforce additional limit stream. Only for debug. Remove on production!!!
                #endif

                //boost::this_thread::sleep(boost::posix_time::milliseconds(30));
              
            }
        }
    }
    delete ddec;
    
    finish = true;
}

void img_process_t(){
    
#if ENABLE_ANAGLYPTH
    Mat left_planes[3], right_planes[3];
    Mat original_3D(Size(1920,1080), CV_8UC3);
#endif
    
    Mat hevc_bgr; // decoded frame result

    cv::cuda::GpuMat l_gpu_orig; 
    cv::cuda::GpuMat l_gpu_transform[2];
    cv::cuda::GpuMat r_gpu_orig;
    cv::cuda::GpuMat r_gpu_transform[2];
    
    cv::cuda::GpuMat leftabgr(output_stream_h, output_stream_w/2, CV_8UC4, Scalar(255,255,255,255));
    cv::cuda::GpuMat rightabgr(output_stream_h, output_stream_w/2, CV_8UC4, Scalar(255,255,255,255));
    
    cv::cuda::Stream cudastream=cv::cuda::Stream::Null();
    
    #if SWAP_CAMERAS
    cv::Rect r_roi_orig(0, 0, input_stream_w/2, input_stream_h);
    cv::Rect l_roi_orig(input_stream_w/2, 0, input_stream_w/2, input_stream_h);
    #else
    cv::Rect l_roi_orig(0, 0, input_stream_w/2, input_stream_h);
    cv::Rect r_roi_orig(input_stream_w/2, 0, input_stream_w/2, input_stream_h);
    #endif

    cv::Rect r_roi_final(0, 0, output_stream_w/2, output_stream_h);
    cv::Rect l_roi_final(output_stream_w/2, 0, output_stream_w/2, output_stream_h);
    
    cv::Mat h_Koef(1, 1, CV_32FC1);  // for memory variables in gpu
    h_Koef.at<float>(0,0) = 0.0000001;  // for memory variables in gpu
    cv::cuda::GpuMat GpuM_koef(h_Koef);  // for memory variables in gpu

    int data_processed = false;
    VideoStabilizer videostabilizer(output_stream_h, (output_stream_w/2));  // class for stabilizing frames
    
    while (!finish){
        data_processed = false;
        for (int i=0; i< MAT_STACK_SIZE; i++ ){  // check for decoded frames
                    if (w_decode_mat_status[i] == 2) {
                        
                        std::chrono::high_resolution_clock::time_point image_processing_time_start = std::chrono::high_resolution_clock::now();
                        
                        cvtColor(w_decode_mat[i], hevc_bgr, COLOR_YUV2BGR_NV12);
                        w_decode_mat[i].release();  // releasing frame
                        w_decode_mat_status[i] = 0; // marking as ready for new input
                        
                        std::chrono::high_resolution_clock::time_point yuv_to_bgr_time_end = std::chrono::high_resolution_clock::now();
                        printf("yuv_to_bgr conversion %ld millis ", std::chrono::duration_cast<std::chrono::milliseconds>( yuv_to_bgr_time_end - image_processing_time_start ).count());
                        
                        
                        #if ENABLE_ANAGLYPTH
                            split(hevc_bgr(l_roi_orig), left_planes);
                            split(hevc_bgr(r_roi_orig), right_planes);
                            Mat in[] = {left_planes[0], left_planes[1], right_planes[2]};
                            int from_to[] = {0, 0, 1, 1, 2, 2};
                            mixChannels(in, 3, &original_3D, 1, from_to, 3);
                            flip(original_3D, original_3D, -1);
                            imshow("anaglympt",original_3D);
                            std::chrono::high_resolution_clock::time_point anaglypth_time_end = std::chrono::high_resolution_clock::now();
                            printf("anaglypth conversion %ld millis ", std::chrono::duration_cast<std::chrono::milliseconds>( anaglypth_time_end - yuv_to_bgr_time_end ).count());
                            
                        #endif
                        
                        // do some frames processing
                        std::chrono::high_resolution_clock::time_point calculations_time_start = std::chrono::high_resolution_clock::now();
                        l_gpu_orig.upload(hevc_bgr(l_roi_orig));
                        cuda::resize(l_gpu_orig, l_gpu_transform[0], Size(output_stream_w/2, output_stream_h), 0, 0, INTER_CUBIC);
                        cuda::flip(l_gpu_transform[0], l_gpu_transform[1], -1);
                        
                        r_gpu_orig.upload(hevc_bgr(r_roi_orig));
                        cuda::resize(r_gpu_orig, r_gpu_transform[0], Size(output_stream_w/2, output_stream_h), 0, 0, INTER_CUBIC);
                        cuda::flip(r_gpu_transform[0], r_gpu_transform[1], -1);

//                        barrelDistort(l_gpu_resized, l_gpu_barrel, GpuM_koef,cudastream);
//                        barrelDistort(r_gpu_resized, r_gpu_barrel, GpuM_koef,cudastream);
//                        bgr_to_abgr(l_gpu_barrel, leftabgr, cudastream);
//                        bgr_to_abgr(r_gpu_barrel, rightabgr, cudastream);

#if STABILIZE_FRAMES
                        std::chrono::high_resolution_clock::time_point stabilization_time_start = std::chrono::high_resolution_clock::now();
                        videostabilizer.stabilizeFrame(r_gpu_transform[1], r_gpu_transform[0], l_gpu_transform[1], l_gpu_transform[0]);

                        std::chrono::high_resolution_clock::time_point calculations_time_end = std::chrono::high_resolution_clock::now();

                        printf("resize %ld millis ", std::chrono::duration_cast<std::chrono::milliseconds>( stabilization_time_start - calculations_time_start ).count());
                        printf("stabilize %ld millis ", std::chrono::duration_cast<std::chrono::milliseconds>( calculations_time_end - stabilization_time_start).count());

                        bgr_to_abgr(l_gpu_transform[0], leftabgr, cudastream);
                        bgr_to_abgr(r_gpu_transform[0], rightabgr, cudastream);
                                          
#else                   
                        bgr_to_abgr(l_gpu_transform[1], leftabgr, cudastream);
                        bgr_to_abgr(r_gpu_transform[1], rightabgr, cudastream);
#endif

                        std::chrono::high_resolution_clock::time_point bgr_to_abgr_time_end = std::chrono::high_resolution_clock::now();
                        
                        printf("resize + bgr_to_abgr %ld millis\n", std::chrono::duration_cast<std::chrono::milliseconds>( bgr_to_abgr_time_end - calculations_time_start).count());
                        
                        // do some frames processing
                        //resize(hevc_bgr, resized_2x, Size(output_stream_w, output_stream_h), 0, 0, INTER_LINEAR);
                        
                        for (int j=0; j < MAT_STACK_SIZE; j++){ // check for encoded frames
                            if (w_encode_mat_status[j] == 0) {

                                leftabgr.download(w_encode_mat[j](l_roi_final));
                                rightabgr.download(w_encode_mat[j](r_roi_final));
                                
                                std::chrono::high_resolution_clock::time_point abgr_donwload_time_end = std::chrono::high_resolution_clock::now();
                        
                                printf("download abgr %ld millis\n", std::chrono::duration_cast<std::chrono::milliseconds>( abgr_donwload_time_end - bgr_to_abgr_time_end).count());
                                printf("TOTAL %ld millis\n", std::chrono::duration_cast<std::chrono::milliseconds>( abgr_donwload_time_end - image_processing_time_start).count());
                                
                                #if ENABLE_ENCODER
                                w_encode_mat_status[j]=1;// enabled encoder
                                #else
                                w_encode_mat_status[j]=0;// disabled encoder
                                //imshow("hevc",w_encode_mat[j]);
                                #endif
                                
                                //imshow("hevc",w_encode_mat[j]);
                                break;
                            }
                        }
                        waitKey(1);
                        data_processed = true;
                    }
                }
        if (!data_processed){  // if no data were processed - wait 10ms
            boost::this_thread::sleep(boost::posix_time::milliseconds(5));
            //printf("waiting for new frame");
        }
    }
}

void main_encode_t(){
    
    int data_encoded = false;
    std::cout << "Startting encoder" << std::endl;

    int iGpu = 0;
    cuInit(0);
    int nGpu = 0;
    cuDeviceGetCount(&nGpu);
    if (iGpu < 0 || iGpu >= nGpu)
    {
        std::cout << "GPU ordinal out of range. Should be within [" << 0 << ", " << nGpu - 1 << "]" << std::endl;
    }
    CUdevice cuDevice = 0;
    cuDeviceGet(&cuDevice, iGpu);

    NV_ENC_BUFFER_FORMAT eFormat = NV_ENC_BUFFER_FORMAT_ABGR;// NV_ENC_BUFFER_FORMAT_IYUV;

    CUcontext cuContext = NULL;
    cuCtxCreate(&cuContext, 0, cuDevice);

    NvEncoderCuda enc(cuContext, output_stream_w, output_stream_h, eFormat);

    NV_ENC_INITIALIZE_PARAMS initializeParams = { NV_ENC_INITIALIZE_PARAMS_VER };
    NV_ENC_CONFIG encodeConfig = { NV_ENC_CONFIG_VER };
    
    initializeParams.encodeConfig = &encodeConfig;

//    initializeParams.frameRateNum = 200;
    enc.CreateDefaultEncoderParams(&initializeParams, NV_ENC_CODEC_H264_GUID, NV_ENC_PRESET_HQ_GUID); // NV_ENC_PRESET_LOW_LATENCY_HQ_GUID
//    initializeParams.frameRateNum = 200;
//    encodeConfig.gopLength = NVENC_INFINITE_GOPLENGTH;
    encodeConfig.frameIntervalP = 1;  // 1-IPP
//    encodeConfig.encodeCodecConfig.h264Config.idrPeriod = NVENC_INFINITE_GOPLENGTH;
    encodeConfig.rcParams.rateControlMode = NV_ENC_PARAMS_RC_VBR_HQ;
    
    enc.CreateEncoder(&initializeParams);
    
    // Params for one frame
    NV_ENC_PIC_PARAMS picParams = {NV_ENC_PIC_PARAMS_VER};
    picParams.encodePicFlags = 0;
    
    printf("starting TCP server");
    
    TCP_stream * tcp_data_stream = new TCP_stream();
    tcp_data_stream->load(1234);
    //tcp_data_stream->setPositionbuffer(&udp_to_station[0]); // set buffer to be filled with marker position
    
    while (!finish){
        
        data_encoded = false;
        for (int i=0; i< MAT_STACK_SIZE; i++ ){
                    if (w_encode_mat_status[i] == 1) {

                        vector<vector<uint8_t>> vPacket;
                        const NvEncInputFrame* encoderInputFrame = enc.GetNextInputFrame();

                        NvEncoderCuda::CopyToDeviceFrame(cuContext, w_encode_mat[i].data, 0, (CUdeviceptr)encoderInputFrame->inputPtr,
                        (int)encoderInputFrame->pitch, enc.GetEncodeWidth(),
                        enc.GetEncodeHeight(), 
                        CU_MEMORYTYPE_HOST, 
                        encoderInputFrame->bufferFormat,0,0);
                        enc.EncodeFrame(vPacket, &picParams);
                        
                        w_encode_mat_status[i] = 0;
                        
                        
                        for (vector<uint8_t> &packet : vPacket)
                        {
                            tcp_data_stream->stream_raw_data(packet.data(), packet.size());
                        }
                        
                        data_encoded = true;
                    }
                }
        if (!data_encoded){  // if no data were processed - wait 10ms
            boost::this_thread::sleep(boost::posix_time::milliseconds(10));
        }

    }
    
}

static bool initialize(){
    w_decode_mat_status[0]=w_decode_mat_status[1]=w_decode_mat_status[2]=0;
    w_encode_mat_status[0]=w_encode_mat_status[1]=w_encode_mat_status[2]=0;
    
    for (int i=0; i< MAT_STACK_SIZE; i++){
        w_encode_mat[i].create(output_stream_h, output_stream_w, CV_8UC4);
    }
    
    return true;
}


int main(int argc, char** argv) {

    if (!initialize()) return 1;
    boost::thread t_Dec(main_decode_t);
    boost::thread t_Proc(img_process_t);
    boost::thread t_Enc(main_encode_t);
    t_Dec.join();
    t_Proc.join();
    t_Enc.join();
    
    return 0;
}

// add additional data to encoded video
// ffmpeg -r 28 -i naujas_gimbal4_h264.mp4 -vcodec copy naujas_gimbal4_h264_28fps.mp4 

